(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench.materials', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('workbench.materials', {
          url: '/materials',
          params: {
            category: null,
            subcategory: null,
            brand: null,
            material: null
          },
          templateUrl: 'app/pages/workbench/materials/index.html',
          controller: 'MaterialsCtrl',
          title: 'Materials',
          sidebarMeta: {
            order: 500,
          }
        });
  }
})();
