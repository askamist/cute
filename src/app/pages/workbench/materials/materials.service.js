(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench.materials')
      .factory('CatalogService', CatalogService);

  /** @ngInject */
  function CatalogService(Restangular) {
    var category = Restangular.all('category'),
        subcategory = Restangular.all('subcategory'),
        brand = Restangular.all('brand'),
        material = Restangular.all('material');
    var catalog = [category, subcategory, brand, material];
    var factory = {
      category: {
        all: category.getList,
        post: category.post,
        delete: function (id){
          return Restangular.one('category', id).remove()
        }
      }
    };
    catalog.forEach(function (ra, i){
      if(i==0)return;
      factory[ra.route] = {
        all: function (id){
          if(id){
            var params = {}
            params[catalog[i-1].route] = id
            return ra.getList(params)
          }
          return ra.getList();
        },
        post: ra.post,
        delete: function (id){
          return Restangular.one(catalog[i].route, id).remove();
        },
      }
    })
    return factory;
  }
})();
