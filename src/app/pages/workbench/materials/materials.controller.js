(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench.materials')
    .controller('MaterialsCtrl', MaterialsCtrl)

  /** @ngInject */
  function MaterialsCtrl($scope, CatalogService, toastr, $stateParams, $state, $uibModal) {
    $scope.catalog = ['category', 'subcategory', 'brand', 'material'];
    $scope.catalog_title = ['Category', 'Sub Category', 'Brand', 'Material'];
    $scope._data = {};
    $scope._is_open = {};
    $scope._is_selected = {};
    $scope._is_disabled = {};
    $scope._selected = $stateParams;

    $scope.catalog.forEach(function (cat, $i) {
      var opened = false;
      $scope._data[cat] = []
      $scope._is_selected[cat] = undefined;
      $scope._is_disabled[cat] = false;
      Object.defineProperty($scope._is_open, cat, {
        get : function(){ return opened; },
        set : function(newValue) {
          opened = newValue;
          if (opened) {
            if($scope._is_disabled[cat])return;
            $scope.catalog.forEach(function (cat, index){
              if(index >= $i){
                $stateParams[cat] = null;
              }
            })
            $state.go('workbench.materials', $stateParams)
          }
        }
      });
    });

    function getSomething(something, filter){
      $scope.loading = true;
      CatalogService[something].all(filter).then(function(data){
        $scope._data[something]= data;
        $scope.loading = false;
      },function (){
        toastr.warning('Please Check your Internet Connection', 'Error loading Data!')
        $scope.loading = false;
      });
    }

    $scope.select = function (cat, id){
      $stateParams[cat] = id;
      $state.go('workbench.materials', $stateParams)
      ProcessParams();
    }

    $scope.add = function ($event, index, edit_mode){
      $event.stopPropagation();
      $event.preventDefault();
      var uibMI = $uibModal.open({
        templateUrl: 'app/pages/workbench/materials/new.html',
        size: 'sm',
        controller: ['$scope', 'CatalogService', function ($modal, CatalogService){
          $modal.data = edit_mode ? edit_mode : {};
          $modal.loading = false;
          $modal.cat = $scope.catalog[index];
          $modal.title = $scope.catalog_title[index];
          $modal.save = function (){
            $modal.loading = true;
            if(index != 0){
              $modal.data[$scope.catalog[index-1]] = $stateParams[$scope.catalog[index-1]];
            }
            (edit_mode ? edit_mode.save() : CatalogService[$scope.catalog[index]].post($modal.data)).then(function (){
              $modal.loading = false;
              ProcessParams();
              uibMI.close();
            })
          }
        }]
      });
    }

    $scope.delete = function (cat, index, item){
      var uibMI = $uibModal.open({
        templateUrl: 'app/pages/shared/templates/confirm_delete.html',
        size: 'sm',
        controller: ['$scope', 'CatalogService', function ($modal, CatalogService){
          $modal.action_name = "Delete";
          $modal.action = function (){
            $modal.loading = true;
            CatalogService[$scope.catalog[index]].delete(item.id).then(function(result){
              toastr.info('The material was successfully Deleted.', 'Material Deleted.')
              ProcessParams();
              uibMI.close();
            }, function(){
              uibMI.close();
              toastr.error('There was error deleting the material. Please Try again', 'Material not Deleted!')
            });
          }
          $modal.cancel = function (){
            uibMI.close();
          }
        }]
      })
    }

    function ProcessParams(){
      var open = false;
      $scope.catalog.forEach(function (cat, index){
        if($stateParams[cat] == null || (index == $scope.catalog.length-1 && $stateParams != null)){
          if(open == false){
            open = $scope._is_open[cat] = true;
            getSomething(cat, $stateParams[$scope.catalog[index - 1]]);
          }else $scope._is_disabled[cat] = true;
        }
      })
    }
    ProcessParams();
  }

})();
