(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench', [
    'CuteAdmin.pages.workbench.projects',
    'CuteAdmin.pages.workbench.materials',
    'CuteAdmin.pages.workbench.porders',
    'CuteAdmin.pages.workbench.checkouts'
  ])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('workbench', {
          url: '/workbench',
          template : '<ui-view/>',
          abstract: true,
          title: 'WorkBench',
          sidebarMeta: {
            icon: 'ion-grid',
            order: 300,
          },
        }).state('workbench.vendors', {
          url: '/vendors',
          templateUrl: 'app/pages/workbench/vendors/tables.html',
          controller: 'WorkbenchCtrl',
          title: 'Vendors',
          sidebarMeta: {
            order: 100,
          },
        });
    $urlRouterProvider.when('/workbench','/workbench/projects');
  }

})();
