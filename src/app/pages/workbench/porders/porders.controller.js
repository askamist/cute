(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench.porders')
    .controller('POrdersCtrl', POrdersCtrl)
    .controller('POrderNewCtrl', POrderNewCtrl)
    .controller('SubPONewCtrl', SubPONewCtrl)

  /** @ngInject */
  function POrdersCtrl($scope, POrdersService, toastr) {
    function getPOrders (){
      $scope.porders = [];
      $scope.loading = true;
      POrdersService.all().then(function(data){
        $scope.porders = data;
        $scope.loading = false;
      },function (){
        toastr.warning('Please Check your Internet Connection', 'Error loading POrders!')
        $scope.loading = false;
      });
    }
    $scope.delete = function (id){
      $scope.loading = true;
      POrdersService.delete(id).then(function(result){
        $scope.loading = false;
        toastr.info('The porder was successfully Deleted.', 'POrder Deleted.')
        getPOrders();
      }, function(){
        toastr.error('There was error deleting the porder. Please Try again', 'POrder not Deleted!')
        $scope.loading = false;
      });
    }
    getPOrders();
  }

  /** @ngInject */
  function POrderNewCtrl($scope, POrdersService, $state, $filter, toastr, projects_list, vendors_list, InventoryService){
    $scope.projects_list = projects_list;
    $scope.vendors_list = vendors_list;
    $scope.materials_list = [];

    $scope.add_material = function (){
      $scope.data.materials.push({})
    }

    $scope.fetch_inventory = function (){
      $scope.load_inventory = true;
      $scope.data.materials = [{}];
      InventoryService.all({project: $scope.data.project}).then(function(data){
        $scope.load_inventory = false;
        $scope.materials_list = data;
      })
    }

    $scope.data = {
      "date_input": new Date(),
      "materials": [{}]
    }
    $scope.save = function (){
      $scope.loading = true;
      $scope.data.expected_date = $filter('date')($scope.data.date_input, "yyyy-MM-dd");
      delete $scope.data['date_input'];

      POrdersService.post($scope.data).then(function(result){
        $scope.loading = false;
        toastr.success('Successfully saved changes porder ' + $scope.data.porder_name, 'POrder Created!')
        $state.go('workbench.porders',{},{reload: true})
      }, function(){
        $state.go('workbench.porders.new',{},{reload: true})
        toastr.error('There was an error creating the porder. Please Try again', 'POrder not Created!')
        $scope.loading = false;
      });
    }
  }

  /** @ngInject */
  function SubPONewCtrl($scope, POrdersService, $state, $filter, toastr, $stateParams){
    $scope.loading = true;
    $scope.PO = null;
    POrdersService.one($stateParams.po).then(function (data){
      $scope.loading = false;
      $scope.PO = data;
    })

    $scope.add_material = function (){
      $scope.data.materials_delivered.push({})
    }

    $scope.data = {
      "date_input": new Date(),
      "materials_delivered": [{}]
    }

    $scope.save = function (){
      $scope.loading = true;
      $scope.data.delivery_date = $filter('date')($scope.data.date_input, "yyyy-MM-dd");
      delete $scope.data['date_input'];
      $scope.data.po = $stateParams.po;

      POrdersService.subpo.post($scope.data).then(function(result){
        $scope.loading = false;
        toastr.success('Successfully created the Sub Po' + $scope.data.porder_name, 'SubPo Created!')
        $state.go('workbench.porders')
      }, function(){
        toastr.error('There was an error creating the sub po. Please Try again', 'Sub Po not Created!')
        $scope.loading = false;
      });
    }
  }
})();
