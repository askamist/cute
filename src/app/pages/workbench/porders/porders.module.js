(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench.porders', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('workbench.porders', {
          url: '/porders',
          views: {
            "": {
              templateUrl: 'app/pages/workbench/porders/master.html',
              controller: 'POrdersCtrl'
            },
            "partial@workbench.porders": {
              templateUrl: 'app/pages/workbench/porders/index.html'
            }
          },
          title: 'POrders',
          sidebarMeta: {
            order: 0,
          }
        })
        .state('workbench.porders.new', {
          url: '/new',
          views:{
            "partial@workbench.porders":{
              templateUrl: 'app/pages/workbench/porders/new.html',
              controller: 'POrderNewCtrl'
            }
          },
          resolve: {
            vendors_list : function (Restangular){
              return Restangular.all('vendor').getList({'basic': ''});
            },
            projects_list : function (ProjectsService){
              return ProjectsService.list();
            }
          },
          title: 'POrders',
          sidebarMeta: {
            order: 0,
          }
        })
        .state('workbench.porders.show', {
          url: '/show/{id:int}',
          views:{
            "partial@workbench.porders":{
              templateUrl: 'app/pages/workbench/porders/show.html',
              controller: function ($scope, POrdersService, $stateParams) {
                $scope.loading = true;
                $scope.porder = {}
                POrdersService.one($stateParams.id).then(function(porder){
                  $scope.po = porder;
                  $scope.loading = false;
                });
                $scope.subpos = {}
                POrdersService.subpo.all({po: $stateParams.id}).then(function(subpos){
                  $scope.subpos = subpos;
                });
              }
            }
          },
          title: 'POrders',
          sidebarMeta: {
            order: 0,
          }
        })
        .state('workbench.porders.newsubpo', {
          url: '/newsubpo?po',
          views:{
            "partial@workbench.porders":{
              templateUrl: 'app/pages/workbench/porders/new_sub_po.html',
              controller: 'SubPONewCtrl'
            }
          },
          title: 'POrders',
          sidebarMeta: {
            order: 0,
          }
        });
  }
})();
