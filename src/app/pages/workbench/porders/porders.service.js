(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench.porders')
    .factory('POrdersService', POrdersService);

  /** @ngInject */
  function POrdersService(Restangular) {
    var porder = Restangular.all('po')
    var subpo = Restangular.all('subpo')
    return {
      all: function all(){
        return porder.getList();
      },
      one: function one(id){
        return porder.get(id);
      },
      post: porder.post,
      save: function(data){
        return data.save();
      },
      delete: function (id){
        return Restangular.one('po', id).remove()
      },
      subpo: {
        all: function all(params){
          return subpo.getList(params || {});
        },
        post: subpo.post
      }
    };
  }

})();
