/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench')
      .controller('WorkbenchCtrl', WorkbenchCtrl);

  /** @ngInject */
  function WorkbenchCtrl($scope, $filter, Restangular, $uibModal) {

    $scope.isLoading = true;

    Restangular.all('vendor').getList().then(function(data){
      $scope.vendors = data;
      $scope.isLoading = false;
    });

    $scope.venders = [];

    $scope.show = function (or, id){
      $uibModal.open({
      animation: true,
      templateUrl: 'app/pages/workbench/'+or+'s/show.html',
      size: 'lg',
      controller: function(){},
      resolve: {
        or: function () {
          return or;
        },
        data: Restangular.one(or, id).get()
      }
    });
    }

  }

})();
