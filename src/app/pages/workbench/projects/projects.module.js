(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench.projects', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('workbench.projects', {
          url: '/projects',
          views: {
            "": {
              templateUrl: 'app/pages/workbench/projects/master.html',
              controller: 'ProjectsCtrl'
            },
            "partial@workbench.projects": {
              templateUrl: 'app/pages/workbench/projects/index.html'
            }
          },
          title: 'Projects',
          sidebarMeta: {
            order: 0,
          }
        })
        .state('workbench.projects.new', {
          url: '/new',
          views:{
            "partial@workbench.projects":{
              templateUrl: 'app/pages/workbench/projects/new.html',
              controller: 'ProjectPostCtrl'
            }
          },
          resolve: {
            project_types: function (Restangular){
              return Restangular.all('projecttype').getList({basic:''});
            },
            clients: function (Restangular){
              return Restangular.all('client').getList({basic:''});
            }
          },
          title: 'Projects',
          sidebarMeta: {
            order: 0,
          }
        })
        .state('workbench.projects.show', {
          url: '/show/{id:int}',
          views:{
            "partial@workbench.projects":{
              templateUrl: 'app/pages/workbench/projects/show.html',
              controller: function ($scope, ProjectsService, $stateParams) {
                $scope.loading = true;
                $scope.project = {}
                ProjectsService.one($stateParams.id).then(function(project){
                  $scope.project = project;
                  $scope.loading = false;
                });
              }
            }
          },
          title: 'Projects',
          sidebarMeta: {
            order: 0,
          }
        })
        .state('workbench.projects.edit', {
          url: '/edit/{id:int}',
          views:{
            "partial@workbench.projects":{
              templateUrl: 'app/pages/workbench/projects/new.html',
              controller: 'ProjectPostCtrl'
            }
          },
          title: 'Projects',
          sidebarMeta: {
            order: 0,
          },
          resolve: {
            project_types: function (Restangular){
              return Restangular.all('projecttype').getList({basic:''});
            },
            clients: function (Restangular){
              return Restangular.all('client').getList({basic:''});
            }
          }
        });
  }
})();
