(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench.projects')
      .factory('ProjectsService', ProjectsService);

  /** @ngInject */
  function ProjectsService(Restangular) {
    var project = Restangular.all('project')
    return {
      all: function all(){
        return project.getList();
      },
      list: function list(){
        return project.getList({'basic': ''})
      },
      one: function one(id){
        return project.get(id);
      },
      post: project.post,
      save: function(data){
        return data.save();
      },
      delete: function (id){
        return Restangular.one('project', id).remove()
      }
    };
  }

})();
