(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench.projects')
    .controller('ProjectsCtrl', ProjectsCtrl)
    .controller('ProjectPostCtrl', ProjectPostCtrl)

  /** @ngInject */
  function ProjectsCtrl($scope, ProjectsService, toastr, $uibModal) {
    function getProjects (){
      $scope.projects = [];
      $scope.loading = true;
      ProjectsService.all().then(function(data){
        $scope.projects = data;
        $scope.loading = false;
      },function (){
        toastr.warning('Please Check your Internet Connection', 'Error loading Projects!')
        $scope.loading = false;
      });
    }
    $scope.delete = function (id){
      var uibMI = $uibModal.open({
        templateUrl: 'app/pages/shared/templates/confirm_delete.html',
        size: 'sm',
        controller: ['$scope', function ($modal){
          $modal.action_name = 'Delete';
          $modal.action = function (){
            ProjectsService.delete(id).then(function(result){
              uibMI.close();
              toastr.info('The project was successfully Deleted.', 'Project Deleted.')
              getProjects();
            }, function(){
              uibMI.close();
              toastr.error('There was error deleting the project. Please Try again', 'Project not Deleted!')
            });
          }
          $modal.cancel = function (){
            uibMI.close();
          }
        }]
      })

    }
    getProjects();
  }

  /** @ngInject */
  function ProjectPostCtrl($scope, ProjectsService, $stateParams, $state, toastr, $filter, Restangular, project_types, clients) {
    $scope.loading = true;
    $scope.data = {}
    $scope.project_types = project_types;
    $scope.clients = clients;
    $scope.consultants_tags = []
    $scope.status = [{'name':'Active','value':'active'},{'name':'Inactive','value':'inactive'}]
    if($stateParams.id){
      ProjectsService.one($stateParams.id).then(function(project){
        $scope.data = project;
        $scope.data.project_type = project.project_type.id;
        $scope.data.date_input = new Date($scope.data.date);
        $scope.data.client = project.client.id;
        $scope.data.status = project.status;
        $scope.consultants_tags = $scope.data.consultants;
        var max_date = new Date();
        max_date.setMonth(11); 
        $scope.max_date = $filter('date')(max_date.setDate(31), "yyyy-MM-dd");
        $scope.loading = false;
      });
    }else{
      $scope.loading = false;
      $scope.data = {
        "enabled": true,
        "date_input": new Date()
      }
    }
    $scope.load_consultants =  function (query){
      return Restangular.all('consultant').getList({basic:''});
    }
    $scope.save = function (isValid){
      if (isValid){
      $scope.loading = true;
      $scope.data.date = $filter('date')($scope.data.date_input, "yyyy-MM-dd");
      delete $scope.data['date_input'];
      $scope.data.consultants = _.map($scope.consultants_tags, function (con){return con.id;});
      ProjectsService[$stateParams.id ? 'save' : 'post']($scope.data).then(function(){
        $scope.loading = false;
        $state.go('workbench.projects.show', {id: $scope.data.id},{reload: true})
        toastr.success('Successfully saved changes project ' + $scope.data.project_name, 'Project Saved!')
      },function(){
        $state.go($state.current, {id: $scope.data.id},{reload: true })
        toastr.error('There was an error saving project. Please Try again', 'Project not Saved!')
        $scope.loading = false;
      });
      }else{
        toastr.error('Please Fix all the Errors!!', 'Project not Saved!')
      }
    }

  }

})();
