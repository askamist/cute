(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench.checkouts', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('workbench.checkouts', {
          url: '/checkouts',
          views: {
            "": {
              templateUrl: 'app/pages/workbench/checkouts/master.html',
              controller: 'CheckoutCtrl'
            },
            "partial@workbench.checkouts": {
              templateUrl: 'app/pages/workbench/checkouts/index.html'
            }
          },
          title: 'Checkouts',
          sidebarMeta: {
            order: 0,
          }
        })
        .state('workbench.checkouts.new', {
          url: '/new',
          views:{
            "partial@workbench.checkouts":{
              templateUrl: 'app/pages/workbench/checkouts/new.html',
              controller: 'CheckoutNewCtrl'
            }
          },
          resolve: {
            vendors_list : function (Restangular){
              return Restangular.all('vendor').getList({'basic': ''});
            },
            projects_list : function (ProjectsService){
              return ProjectsService.list();
            },
            materials_list : function (CatalogService){
              return CatalogService.material.all();
            }
          },
          title: 'Checkout',
          sidebarMeta: {
            order: 0,
          }
        })
        .state('workbench.checkouts.show', {
          url: '/show/{id:int}',
          views:{
            "partial@workbench.checkouts":{
              templateUrl: 'app/pages/workbench/checkouts/show.html',
              controller: function ($scope, CheckoutsService, $stateParams) {
                $scope.loading = true;
                $scope.checkout = {}
                CheckoutsService.one($stateParams.id).then(function(checkout){
                  $scope.co = checkout;
                  $scope.loading = false;
                });
              }
            }
          },
          title: 'Checkout',
          sidebarMeta: {
            order: 0,
          }
        })
        .state('workbench.checkouts.edit', {
          url: '/edit/{id:int}',
          views:{
            "partial@workbench.checkouts":{
              templateUrl: 'app/pages/workbench/checkouts/new.html',
              controller: 'CheckoutEditCtrl'
            }
          },
          title: 'Checkout',
          sidebarMeta: {
            order: 0,
          }
        });
  }
})();
