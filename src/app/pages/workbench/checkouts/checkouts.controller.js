(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench.checkouts')
    .controller('CheckoutCtrl', CheckoutCtrl)
    .controller('CheckoutNewCtrl', CheckoutNewCtrl)

  /** @ngInject */
  function CheckoutCtrl($scope, CheckoutsService, toastr) {
    function getCheckouts (){
      $scope.checkouts = [];
      $scope.loading = true;
      CheckoutsService.all().then(function(data){
        $scope.checkouts = data;
        $scope.loading = false;
      },function (){
        toastr.warning('Please Check your Internet Connection', 'Error loading POrders!')
        $scope.loading = false;
      });
    }
    $scope.delete = function (id){
      $scope.loading = true;
      CheckoutsService.delete(id).then(function(result){
        $scope.loading = false;
        toastr.info('The porder was successfully Deleted.', 'POrder Deleted.')
        getCheckouts();
      }, function(){
        toastr.error('There was error deleting the porder. Please Try again', 'POrder not Deleted!')
        $scope.loading = false;
      });
    }
    getCheckouts();
  }

  /** @ngInject */
  function CheckoutNewCtrl($scope, CheckoutsService, $state, $filter, toastr, projects_list, vendors_list, InventoryService){
    $scope.projects_list = projects_list;
    $scope.vendors_list = vendors_list;
    $scope.materials_list = [];

    $scope.data = {
      "date_input": new Date(),
    }

    $scope.fetch_inventory = function (){
      $scope.load_inventory = true;
      $scope.data.materials = [{}];
      InventoryService.all({project: $scope.data.project}).then(function(data){
        $scope.load_inventory = false;
        $scope.materials_list = data;
      })
    }

    $scope.update_quantity = function (){
      var q = $scope.materials_list[$scope.data.material].quantity;
      $('.inputquantity').val(q).attr('max',q);
    }

    $scope.save = function (){
      $scope.loading = true;
      $scope.data.date = $filter('date')($scope.data.date_input, "yyyy-MM-dd");
      delete $scope.data['date_input'];

      CheckoutsService.post($scope.data).then(function(result){
        $scope.loading = false;
        toastr.success('Successfully saved changes porder ' + $scope.data.porder_name, 'Checkout Done!')
        $state.go('workbench.checkouts',{},{reload: true})
      }, function(){
        $state.go($state.current,{},{reload: true})
        toastr.error('There was an error creating the porder. Please Try again', 'Checkout Failed!')
        $scope.loading = false;
      });
    }
  }
})();
