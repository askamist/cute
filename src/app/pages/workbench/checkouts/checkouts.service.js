(function () {
  'use strict';

  angular.module('CuteAdmin.pages.workbench.checkouts')
      .factory('CheckoutsService', CheckoutsService);

  /** @ngInject */
  function CheckoutsService(Restangular) {
    var checkout = Restangular.all('checkout')
    return {
      all: function all(){
        return checkout.getList();
      },
      one: function one(id){
        return checkout.get(id);
      },
      post: checkout.post,
      save: function(data){
        return data.save();
      },
      delete: function (id){
        return Restangular.one('po', id).remove()
      }
    };
  }

})();
