(function () {
  'use strict';

  angular.module('CuteAdmin.pages')
    .directive('maxwordcount', maxWordCount);

  /** @ngInject */
  function maxWordCount() {
    return {
      require: 'ngModel',
      link: function(scope, elm, attr, ctrl) {
        if(!ctrl) return;

        var maxwords = -1;
        attr.$observe('maxwordcount', function(value) {
          var intVal = parseInt(value);
          maxwords = isNaN(intVal) ? -1 : intVal;
          ctrl.$validate();
        });

        ctrl.$validators.maxwordcount = function(modelValue, viewValue){
          if(ctrl.$isEmpty(modelValue)) return true;
          return (maxwords < 0) || ctrl.$isEmpty(viewValue) || viewValue.split(' ').length >= maxwords;
        };
      }
    };
  }
})();
