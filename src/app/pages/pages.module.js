(function () {
  'use strict';

  angular.module('CuteAdmin.pages', [
    'ui.router',
    'restangular',
    'angularSpinner',

    'CuteAdmin.pages.dashboard',
    'CuteAdmin.pages.workbench',
    'CuteAdmin.pages.inventory',
  ])
  .config(appConfig);

  /** @ngInject */
  function appConfig($urlRouterProvider, RestangularProvider) {
    $urlRouterProvider.otherwise('/dashboard');

    RestangularProvider.setBaseUrl('http://www.linkartica.com:8000');
    RestangularProvider.addResponseInterceptor(function (data, operation){
      var mData;
      if(operation == "getList"){
        mData = data["results"];
        mData.meta = {count: data["count"], type: operation}
      }
      else if(operation == "remove"){
        return data;
      }
      else{
        mData = data;
        mData.meta = {type: 'get'}
      }
      return mData;
    })
    .setRequestSuffix('/')
  }

})();
