(function () {
  'use strict';

  angular.module('CuteAdmin.pages.inventory')
      .controller('InventoryCtrl', InventoryCtrl);

  /** @ngInject */
  function InventoryCtrl($scope, InventoryService) {
    function getInventory(){
      $scope.inventory = [];
      $scope.loading = true;
      InventoryService.all().then(function(data){
        $scope.inventory = data;
        $scope.loading = false;
      },function (){
        toastr.warning('Please Check your Internet Connection', 'Error loading the Inventory!')
        $scope.loading = false;
      });
    }
    getInventory();
  }

})();
