(function () {
  'use strict';

  angular.module('CuteAdmin.pages.inventory', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('inventory', {
          url: '/inventory',
          templateUrl: 'app/pages/inventory/index.html',
          controller : 'InventoryCtrl',
          title: 'Inventory',
          sidebarMeta: {
            icon: 'ion-clipboard',
            order: 200,
          },
        });
  }

})();
