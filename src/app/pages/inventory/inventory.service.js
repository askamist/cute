(function () {
  'use strict';

  angular.module('CuteAdmin.pages.inventory')
      .factory('InventoryService', InventoryService);

  /** @ngInject */
  function InventoryService(Restangular) {
    var project = Restangular.all('inventory')
    return {
      all: function all(params){
        return project.getList(params || {});
      }
    };
  }

})();
